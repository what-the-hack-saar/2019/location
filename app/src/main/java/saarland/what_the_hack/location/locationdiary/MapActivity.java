package saarland.what_the_hack.location.locationdiary;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;

import org.mapsforge.map.android.util.AndroidUtil;
import org.mapsforge.map.datastore.MapDataStore;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.rendertheme.InternalRenderTheme;
import org.mapsforge.map.android.view.MapView;

import java.io.File;

public class MapActivity extends AppCompatActivity {

    private static final String MAP_FILE = "germany.map";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT > 22) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION},
                    234324234);
        }

        setContentView(R.layout.activity_map);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        AndroidGraphicFactory.createInstance(getApplication());

        MapView mapView = findViewById(R.id.mapView);

        try {
            /*
             * We then make some simple adjustments, such as showing a scale bar and zoom controls.
             */
            mapView.setClickable(true);
            mapView.getMapScaleBar().setVisible(true);
            mapView.setBuiltInZoomControls(true);

            /*
             * To avoid redrawing all the tiles all the time, we need to set up a tile cache with an
             * utility method.
             */
            TileCache tileCache = AndroidUtil.createTileCache(this, "mapcache",
                    mapView.getModel().displayModel.getTileSize(), 1f,
                    mapView.getModel().frameBufferModel.getOverdrawFactor());

            /*
             * Now we need to set up the process of displaying a map. A map can have several layers,
             * stacked on top of each other. A layer can be a map or some visual elements, such as
             * markers. Here we only show a map based on a mapsforge map file. For this we need a
             * TileRendererLayer. A TileRendererLayer needs a TileCache to hold the generated map
             * tiles, a map file from which the tiles are generated and Rendertheme that defines the
             * appearance of the map.
             */
            File mapFile = new File(Environment.getExternalStorageDirectory(), MAP_FILE);
            MapDataStore mapDataStore = new MapFile(mapFile);
            TileRendererLayer tileRendererLayer = new TileRendererLayer(tileCache, mapDataStore,
                    mapView.getModel().mapViewPosition, AndroidGraphicFactory.INSTANCE);
            tileRendererLayer.setXmlRenderTheme(InternalRenderTheme.DEFAULT);

            /*
             * On its own a tileRendererLayer does not know where to display the map, so we need to
             * associate it with our mapView.
             */
            mapView.getLayerManager().getLayers().add(tileRendererLayer);

            /*
             * The map also needs to know which area to display and at what zoom level.
             * Note: this map position is specific to Berlin area.
             */
            mapView.setCenter(new LatLong(49.3728, 7.187));
            mapView.setZoomLevel((byte) 16);

//            Intent i= new Intent(this, ScreenService.class);
//            startService(i);

        } catch (Exception e) {
            e.printStackTrace();
        }

        final TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            if (Build.VERSION.SDK_INT > 22) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            234324235);
            }
            final GsmCellLocation location = (GsmCellLocation) telephony.getCellLocation();
            if (location != null) {
                String networkOperator = telephony.getNetworkOperator();
                String mcc = networkOperator.substring(0, 3).replaceFirst ("^0*", ""); // mcc
                String mnc = networkOperator.substring(3).replaceFirst ("^0*", ""); // net
                String lac = Integer.toString(location.getLac()); //area
                String cid = Integer.toString(location.getCid()); //cell
                System.out.println("LAC: " + lac + " CID: " + cid + " MCC: " + mcc + " MNC: " + mnc);

                SQLiteDatabase db = SQLiteDatabase.openDatabase("/storage/emulated/0/mls.db", null, SQLiteDatabase.OPEN_READONLY);

                try {
                    Cursor c;
                    c = db.rawQuery("select lon, lat from locations where mcc=? AND net=? AND area=? AND cell=?", new String[] {mcc, mnc, lac, cid });
                    c.moveToFirst();
                    String lon = c.getString(c.getColumnIndex("lon"));
                    String lat = c.getString(c.getColumnIndex("lat"));
                    System.out.println("LON: " + lon + " LAN: " + lat);
                    //mapView.setCenter(new LatLong(Float.valueOf(lat), Float.valueOf(lon)));

                    c.close();
                }
                catch(Exception e) {
                    System.out.println(e.getStackTrace());
                    System.out.println("No location found!");
                }


            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
